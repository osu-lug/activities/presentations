---
title: Pimp Your Shell
author:
  - Robert Detjens
  - OSU LUG
date: 2/8/22

theme: black
controls: false
hash: true

width: 1600
height: 900
---

# Default Shell

Usually boring.

![CentOS :ResidentSleeper:](images/centos8.png)

---

Debian/etc at least has some color

and real basic tab completion

![](images/debian11.png)

![](images/ubuntu.png)

# We can do better

Its 2022, make your shell pop!

![](images/ls-cat.png)

# How?

stock coreutils are so 1976

- `ls` -> **`lsd`** (or `colorls` or `exa`)
- `cat` -> **`bat`**
- `diff` -> **`delta`**
- `find` -> **`fd`**
- **`fzf`**

don't forget the shell either

:::::: {.columns}
::: {.column}

Bash:

- custom `$PS1`
- `bashgit`

:::
::: {.column}

ZSH:

- themes
- plugins

:::
::::::

## lsd

`lsd` is a "next gen ls command": <https://github.com/Peltoche/lsd>

![](images/lsd.png)

icons, colors, sizes, `--tree`

## bat

`cat` on steroids: <https://github.com/sharkdp/bat>

syntax highlighting, paging, git integration

![](https://camo.githubusercontent.com/7b7c397acc5b91b4c4cf7756015185fe3c5f700f70d256a212de51294a0cf673/68747470733a2f2f696d6775722e636f6d2f724773646e44652e706e67)

`alias cat='bat -pp'`

## delta

`diff`, but better: <https://github.com/dandavison/delta>

better color, context, highlighting

![](images/delta.png)

## fzf

find everything super fast with fuzzy search: <https://github.com/junegunn/fzf>

has shell bindings for `^T` and `^R`

![](images/fzfcd.png)
![](images/fzfhist.png)

## Other stuff

<https://github.com/ibraheemdev/modern-unix>

# Bash customization

not too much here that I've seen, but there is some!

## Custom PS1

`$PS1` is what tells the shell how to make the prompt

builder sites: <https://bashrcgenerator.com/>, <https://ezprompt.net/>

![](images/bashps1.png)

## Bashgit

Shows the current branch & status in the prompt: <https://github.com/oyvindstegard/bashgit>

![](images/bashgit.png)

just source it in your `~/.bashrc`

# Zsh

![](images/zsh.png)

## Plugins

Oh-My-Zsh <https://github.com/ohmyzsh/ohmyzsh>

Big plugin and theme collection, easy to install and use, very pog

but: can be kinda cumbersome to use outside plugins

## Plugin Managers

Faster and more flexible than just OMZ

- **Antigen** (the incumbent) <https://github.com/zsh-users/antigen>
  - fast, zsh-native, been around for years
- **Sheldon** (what I use) <https://github.com/rossmacarthur/sheldon>
  - super fast, in Rust, uncluttered zshrc

## Themes

p10k is king

colors, multiline, git status, env description

<https://github.com/romkatv/powerlevel10k>

![](images/zsh.png)

## My plugin/theme recs:

- `ael-code/zsh-colored-man-pages`
- `MichaelAquilina/zsh-autoswitch-virtualenv`
- `zdharma-continuum/fast-syntax-highlighting`
- `zsh-users/zsh-autosuggestions`
- `zsh-users/zsh-completions`
- `zsh-users/zsh-history-substring-search`
- `romkatv/powerlevel10k`
- `ohmyzsh/ohmyzsh` -> `fzf`
