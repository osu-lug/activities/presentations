---
title: Officer Positions for 2022,3
author:
  - OSU LUG
date: 3/29/22

theme: black
controls: false
hash: true

width: 1600
height: 900
---

# Plans for this term!

- more talks?
  - Gaming on Linux -- Proton, Lutris, etc
  - Systemd and Service Management
  - Pipewire and Linux Sound
  - Using GitHub features effectively -- PRs, Actions, etc
  - Pimp My Editor
- more activities?
  - Gabriel's custom keyboard build

# Officer Positions

Being an officer is a fun experience, and pretty low key overall!

- Plan meetings: make talks, organize guest speakers, etc
- Logistics: room reservations
- Infra management: keep the LUG servers running and set up any new services

## General Responsibilities

We've been sharing the load between the three of us this year, but generally:

<style>
  table {
    font-size: smaller;
    color: lightgray;
  }
</style>

|                    |                                                                                                                              |
|--------------------|------------------------------------------------------------------------------------------------------------------------------|
| **President**      | The person in charge! Gets to choose the club's direction, schedules meetings, decides on projects to take on, etc           |
| **Vice President** | The ultimate supporter. Helps the president come up with ideas, and really does anything they think will be helpful.         |
| **Treasurer**      | In charge of the club's finances. Next year we should have money to get pizza again!                                         |
| **Secretary**      | Writes club wide communications, and in general handling things like taking notes if necessary and contacting collaborators. |

## Interested?

Fill out this form:

![<https://oregonstate.qualtrics.com/jfe/form/SV_ebpWJnlbjT2IpHU>](survey-qrcode.png)
