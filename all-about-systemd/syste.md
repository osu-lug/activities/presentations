---
title: All About SystemD
author:
  - Robert Detjens
  - OSU LUG
date: 4/5/2022

theme: black
controls: false
hash: true

width: 1600
height: 900
---

# What is `systemd`?

PID 1: system init

Sets up the system

provides a "system layer"

# Some History

## What is `init`?

```
INIT(8) 						      System Manager's Manual							   INIT(8)

NAME
        init, rc  -  process control initialization

DESCRIPTION
        Init is invoked as the last step of the boot procedure (see boot(8)).  Generally its role
        is to create a process for each typewriter on which a user may log in.

        (...)

        When init comes up multiuser, it invokes a shell, with input taken from the file /etc/rc.
        This command file performs housekeeping like removing temporary files, mounting file
        systems, and starting daemons.

```

## Unix was simple.

![Seventh Edition Unix on a PDP-11](images/pdp1170unix.png){ height=500px }

---

![BSD 4 on a VAX 11/730](images/vax11730bsd.png){ height=500px }

---

:::::: {.columns}
::: {.column width="50%"}

`init`

- started getty for TTYs ( `Ct+Al+F#` )
- mounted filesystems
- ran shell scripts under `/etc/init.d`
- walked away

\

`/etc/init.d`

- all shell scripts -->
- relied on double forking
- everything handled manually
- `/etc/init.d/httpd restart`
- `service httpd restart`

:::
::: {.column width="50%"}

```sh
#!/bin/bash

start() {
  # start app
}

stop() {
  # stop app
}

case "$1" in
    start)
       start
       ;;
    stop)
       stop
       ;;
    restart)
       stop; start
       ;;
    status)
       # do something to report status
       ;;
    *)
       echo "Usage: $0 {start|stop|restart|status}"
esac
```

:::
::::::

## Problems

- crash handling
- dependencies
- logging
- slow
- **things changed**

## Things changed

- the internet
- persistence
- communication
- lifecycle
- daemon -> service

## Systemd provides...

- dependency handling
- resource tracking
- multiple types of services
- parallel starting
- automatic mounting
- delayed activation
- **system housekeeping**

# How do I use it?

`systemctl`!

|                         |                                                        |
|-------------------------|--------------------------------------------------------|
| See whats running:      | `systemctl status` \n `systemctl list-units`           |
| Start/stop a service:   | `systemctl start httpd` \n `systemctl stop  httpd`     |
| Start/stop on boot:     | `systemctl enable  httpd` \n `systemctl disable httpd` |
| Show info on a service: | `systemctl status httpd`                               |
| Show dependencies:      | `systemctl list-dependencies httpd`                    |

\+ lots of other stuff, see later

# Other types of units

| Unit Type     | File Extension | Description                            |
|---------------|----------------|----------------------------------------|
| Service unit  | `.service`     | A system service / daemon.             |
| Scope unit    | `.scope`       | An externally created process.         |
| Slice unit    | `.slice`       | A resource tracking group              |
| Snapshot unit | `.snapshot`    | A saved state of the systemd manager.  |
| Socket unit   | `.socket`      | An inter-process communication socket. |
| Swap unit     | `.swap`        | A swap device or a swap file.          |
| Timer unit    | `.timer`       | A systemd timer.                       |

## Writing units

:::::: {.columns}
::: {.column width="60%"}

- `[Unit]`
  - generic options
  - description, dependencies, etc

- `[Unit Type]`
  - type-specific directives, e.g. `[Service]`
  - what to execute for `.service`
  - what to mount for `.mount`
  - etc

- `[Install]`
  - information about unit installation
  - used by `systemctl enable` and `disable`

:::
::: {.column width="40%"}

```ini
# /etc/systemd/system/the-bishop.service
[Unit]
Description=The Bishop Discord Bot
After=network-online.target
Wants=network-online.target

[Service]
Type=simple
WorkingDirectory=/var/bots/the-bishop/
ExecStartPre=bundle install --deployment
ExecStart=bundle exec ruby main.rb
Restart=always
User=bots

[Install]
WantedBy=multi-user.target
```

:::
::::::

## Socket Activation

Don't start the service until its needed!

Both local unix sockets and network ports supported

:::::: {.columns}
::: {.column width="50%"}

```ini
# /etc/systemd/systemd/foo.socket
[Unit]
Description=Foo Socket
PartOf=foo.service

[Socket]
ListenStream=1234
ListenStream=/run/foo.socket

[Install]
WantedBy=sockets.target
```

:::
::: {.column width="50%"}

```ini
# /etc/systemd/systemd/foo.service
[Unit]
Description=Foo Service
After=network.target foo.socket
Requires=foo.socket

[Service]
Type=simple
ExecStart=/usr/bin/python3 /somewhere/serve.py

# this is optional! service doesn't need to be enabled
[Install]
WantedBy=multi-user.target
```

:::
::::::

## Parameterized / Instantiated units

This is what the `@` is

```ini
# /usr/lib/systemd/system/getty@.service
[Unit]
Description=Getty on %I
...
[Service]
ExecStart=-/sbin/agetty --noclear %I $TERM
...
```

|              |                                             |
|--------------|---------------------------------------------|
| `%n`         | Full unit name (i.e. `getty@ttyS1.service`) |
| `%p`         | Prefix name (i.e. `getty`)                  |
| `%i` or `%I` | Instance name (i.e. `ttyS1`)                |
| `%H`         | Host name                                   |

# System journal: `journalctl`

|                                 |                                      |
|---------------------------------|--------------------------------------|
| Look at the latest messages:    | `journalctl -e`                      |
| Watch the latest messages:      | `journalctl -f/--follow`             |
| Log from previous boots:        | `journalctl -b/--boot -1`            |
| Only show output from one unit: | `journalctl -u/--unit httpd.service` |
| Grep for stuff:                 | `journalctl -g/--grep something`     |

# User sessions

Useful for running user-level daemons, e.g. the DE, noise cancelling

same commands the system sessions, with `--user`

- `systemctl --user ...`
- `journalctl --user ...`

Units under `~/.config/systemd/user/`

# Other cool stuff systemd can do

## `systemd-cgtop`

See resource usage per-service, and per-app!

![](images/cgtop.png)

## `systemd-analyze`

See what services are taking the longest

![`systemd-analyze blame`]()

Make a chart, even

![`systemd-analyze plot`]()

---

# Further resources

- [The Tragedy of systemd](https://www.youtube.com/watch?v=o_AIw9bGogo), great talk on the history and controversy of systemd
- [CS312 systemd slides](https://cs312.osuosl.org/slides/08_systemd.html)
- [Rethinking PID 1](https://0pointer.de/blog/projects/systemd.html), Poeterring's initial post about systemd
- [Official site + documentation](https://www.freedesktop.org/wiki/Software/systemd/)
