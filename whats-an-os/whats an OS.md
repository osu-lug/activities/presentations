---
title: Operating Systems? huh?
author: Gabriel Kulp
date: November 22, 2021
theme: white
transition: concave
progress: false
controls: false
---

# Questions

- What is an OS?
- When might you want an OS?
- When is an OS an option?

# 1/3: What is an OS?

Broad, so I'll narrow it

---

## Basically

> An OS multiplexes computing needs onto computing resources.

This requires a kernel.

---

## What's a kernel?

- Self-initializing
- Collection of ISRs
- Enforce multitasking
- Handle virtual memory

---

## What does it do?

- Handles privileged tasks
- Scheduling
- Drivers (sometimes)

---

# 2/3: When might you want an OS?

---

## Goals

- Security
- Privilege separation
- Memory safety
- Abstraction

---

## Examples

- Networking
- Process and thread separation
- Isolated failure
- Portability

# 3/3: When is an OS an option?

---

## Hardware requirements

- Software interrupt with privilege switch
- Virtual memory (pages, page faults)
- Atomic instructions (if SMT or MC)

---

## Interrupts

- Context switching
- Preemptive multitasking
- Page faults

---

## Virtual Memory

- Address space separation
- MMU and page tables
- Context switching

---

## Atomic Instructions

- If processes and threads move
- Not everything can be concurrent
- Hardware requirement for atomics
- Hard with pipelines, OoOE, caching

---

# Questions?

I am not an expert.
