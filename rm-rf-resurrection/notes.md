# Recovering from `rm -rf /`

## The scenario

Let's say you're working from home on a weekend via SSH.
You type something wrong.
Maybe instead of `rm -rf /lost+found` you accidentally type `rm -rf / lost+found`.
Then, for some reason, you ignore the warning it gives you, and do it anyway.

Let's say you don't have easy physical access to this machine, but you have backups somewhere.
So you need a way to get it just functional enough to 

## The setup
```sh
rm -rf / --no-preserve-root --permission-to-kill-kittens-explicitly-granted
```

Now what? Let's take inventory.

No files: executables, configs, libraries, the dynamic ELF loader, etc.

No `init` or kernel, so no hope of rebooting.

Lots of stuff still in RAM: entire kernel is fine, pseudo filesystems like `/proc` and `/dev` can't be killed this way (expert mode: `umount` everything so you have an empty filesystem).

Probably lots of processes already crashed or quit.
We can sorta look at them in `/proc`. 
We still have `sshd` which doesn't touch disk once you're logged in, and of course `bash` that we're running right now.
Wait a sec, why does `cd` work?

## Basic navigation and interaction

Press `cd` and then hit tab once or twice.
Clearly the shell can read the filesystem, even though we don't have `ls`!
Another example is to take advantage of what's called "globbing", which is when you use `*` in a command to auto-fill every matching name.
Something like `cp *.txt dest`.
It turns out the shell makes the substitution before calling the command, so that means the shell can (on its own) read the filesystem for matching filenames!
That means you can do `echo *` to see everything in the current directory.

This is because `echo` is a shell builtin, meaning it's a function that's already loaded, without needing to fetch and execute a file from disk.
List all enabled builtins with `enable`.
These are our friends right now.

`echo` and redirection still work, so we can write files with `echo "content" > file.txt`

```sh
alias ls="echo *"

# cat is like this, but can't alias
echo `</proc/self/io`

# instead, we can make a custom function
# (but this won't actually be needed)
cat() {
echo `<"$1"`
}
```

`echo` will print out raw hex values in the `\x00` format with `-e` and will suppress newlines with `-n` so use `echo -en "\x87\x0a"`.
Dump binary data into this format with this handy Python one-liner that I put in a script I call `echo-dump`:

```python
sys.stdout.write(''.join(['\\x%02x'%b for b in sys.stdin.buffer.read()]))
```

Use this script on the host to convert any file to the format needed for `echo -en` to recreate it:

```sh
echo-dump < file | wl-copy
```

(Or pipe to `xclip -c` if you're using X11. `wl-copy` is a tool to interact with the Wayland clipboard)
Then paste it between the quotes here in the remote machine to recreate that file:

```sh
echo -en "" > file
```

## Building up executables

We can write files. Executables, even. But can't actually execute them. No `chmod +x`, so nothing we make will be runnable. Sideload `busybox` as an example.

- Option 1: maybe find something already executable and overwrite it
	- this would be by memory or trial-and-error without `ls` or `find`
- Option 2: inject code into the currently-running `bash` process
	- sounds much more fun

Bash has builtins. And you can actually load more at runtime! They're loaded from a `.so` file which doesn't have to be executable! So we can write that file using the tricks above, then load a new builtin from it to run arbitrary code. What code to run? Why of course we want `chmod +x`!

Building custom builtins is super annoying. Took like two hours to figure out how. Not very common, I suppose! The example that finally worked is [here](https://mbuki-mvuki.org/posts/2021-07-12-writing-a-bash-builtin-in-c-to-parse-ini-configs/).

The code to actually add the execute permission looks like this:

```c
#include <builtins.h>
#include <shell.h>
#include <bashgetopt.h>
#include <common.h>
#include <errno.h>

#include <stdio.h>
#include <sys/stat.h> // chmod is defined here

// implementation of new builtin function
int addx_builtin(WORD_LIST * list) {
	if (!list) {
		builtin_usage();
		return EX_USAGE;
	}
	puts("Adding execution permissions to this file:");
	puts(list->word->word);
	return chmod(list->word->word, S_IXUSR | S_IXGRP | S_IXOTH);
}

// documentation
char *addx_doc[] = {
	"addx FILE",
	"Adds execute permissions to FILE. Equivalent to chmod +x FILE",
	(char *)NULL
};

// object that bash loads to represent the builtin
struct builtin addx_struct = {
	.name = "addx", // name of builtin
	.function = addx_builtin, // function implementing it
	.flags = BUILTIN_ENABLED, // initial flags for builtin
	.long_doc = addx_doc,
	.short_doc = "addx FILE", // usage
	.handle = 0 // reserved for internal use
};
```

Compile with this command:

```sh
gcc -shared -Os -s addx.c -o addx.so $(pkgconf --cflags bash)
# -Os and -s optional. They shrink the final binary size
```

Sideload with `echo-dump`, then enable with `enable -f ./addx.so addx`! The first argument is the file to load from, and the second is the command to load from it.

## Route 1: Bootstrap with `busybox`

Download `busybox` and sideload it with `echo-dump` as shown above. You can get it [here](https://www.busybox.net/downloads/binaries/).

Make `busybox` executable with new builtin and then have it install itself:

```sh
/busybox mkdir -p /usr/local/bin
/busybox mv busybox /usr/local/bin
cd /usr/local/bin
## optional: manually install only the commands you need
# ./busybox ln -s ./busybox /usr/local/bin/ln
# ln -s ./busybox /usr/local/bin/ls
# ls
# ln -s ./busybox /usr/local/bin/mv
# mv test test2
# ln -s ./busybox /usr/local/bin/cat
# cat test2
./busybox --install -s /usr/local/bin
wget # nice
ash -l # start a new shell for fun
```

Now we have lots of commands and this is suddenly much more comfortable. Importantly, we have `wget`, `tar`, `mount`, `vi`, and `dd`.
These are likely essential to any recovery operations you need to do next.

Another possibly-helpful utility is `pivot_root`, which allows you to change the root of the filesystem to point to some new spot, like a RAM disk you just mounted, or a backup drive.


## Route 2: Install full userspace in-place

Option 1: [install Arch in place with normal install process](https://wiki.archlinux.org/title/Install_Arch_Linux_from_existing_Linux)

Option 2: [install Alpine in place via rootfs tarball](https://wiki.alpinelinux.org/wiki/Bootstrapping_Alpine_Linux)

Option 3: [install Debian in place with `debootstrap`](https://wiki.debian.org/Debootstrap)

Let's do option 2 since Alpine is pretty simple.

You'll need to get `apk-tools-static` downloaded and executable, and we can use it to install every other package, including `openssh-server`, `linux-lts`, and `grub`.

At the time of writing, the download is [here](https://dl-cdn.alpinelinux.org/alpine/edge/main/x86_64/apk-tools-static-2.12.9-r1.apk).
Likely the version number will change and break that link pretty quickly, so just search for `apk-tools-static` on [this](https://dl-cdn.alpinelinux.org/alpine/edge/main/x86_64/) page.

Now either send this file to the remote machine if you already have `tar` usable (via Busybox probably), else extract it locally and only send the `apk.static` binary over.

```
tar zxf apk-tools-static-*.apk
./sbin/apk.static --arch x86_64 -X "http://dl-cdn.alpinelinux.org/alpine/latest-stable/main/" -U --allow-untrusted --root / --initdb add alpine-base
```

Next, add the following to `/etc/apk/repositories`:
```
http://dl-cdn.alpinelinux.org/alpine/edge/main
http://dl-cdn.alpinelinux.org/alpine/edge/community
```

Then, run this command so the remote computer knows how to resolve domain names to IP addresses: `echo nameserver 1.1.1.1 > /etc/resolv.conf`.

Finally, you can run `apk update`, and `apk add openssh-server linux-lts grub` to get some essentials.
Enable the SSH server at boot with `rc-update add sshd`.
Don't forget to set up host keys with `ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa` and put your own key in `/root/.ssh/authorized_hosts` (or enable password login in `/etc/ssh/sshd_config`).
If you choose an unused port in `sshd_config`, you can run this new SSH server at the same time as the service that's still running from the dead system, but there are some extra hoops to make it work as expected before a reboot.
I didn't go through all the steps, but the gist is that you'll need to set up something in `/dev` to allow PTY allocation.

In a similar vein, if you want to install `grub`, you'll need access to `/dev/sda` and friends.
These types of device files are typically auto-generated by the `udev` service, but it's borked, so we'll need to do it manually with the `mknod` command.
This command can make special files, including ones that offer an interface to how the kernel handles hard drives, SSDs, and the like.
The syntax is `mknod /dev/<device> <major> <minor>`, where the values of device, major, and minor can be found in the kernel pseudo-filesystem with `cat /proc/partitions`.
For example, on my demo VM, it was `mknod /dev/vda 254 0`, `mknod /dev/vda1 254 1`.
Once everything is in place, you can try `grub-mkconfig` and then `grub-install /dev/vda` (or whatever device you just set up with `mknod`).

Hopefully, after this, everything just works!


## Route 3: Overwrite HDD in-place

Note that this solution will likely severely limit the data recovery options at your disposal.
Also note that I haven't actually tried this so I don't know what pitfalls there are.

You know how you can download a `.iso` file, copy it to a flash drive, and boot a Linux distro with it?
Well you can do the exact same thing on an HDD or SSD!

So just pick a distro and download a live image for it, then use `mknod` as described above to recreate the appropriate device file, and finally `dd if=newdistro.iso of=/dev/sda`!
Then, edit some stuff to make sure the SSH server starts automatically and you can log in.

If you happen to have a full-disk backup of your machine, you can just get `busybox` as described above, and use `wget` or `nc` to download the backup, and maybe `dd` to apply it.
It could look something like this on the remote system: `nc -lp 12345 > /dev/sda` and then like this on your functioning system: `cat backup.iso | nc <address> 12345`.

If you first download the `.iso` and then apply it with `dd`, make sure you don't download it onto the same drive you intend to overwrite, or weird things will probably happen.


## Finally

Once you think you might have set things up enough, you can reboot!
Wait a minute and try to connect again, and with buckets of luck you might just connect to your new system!

