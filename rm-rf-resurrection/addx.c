#include <builtins.h>
#include <shell.h>
#include <bashgetopt.h>
#include <common.h>
#include <errno.h>

#include <stdio.h>
#include <sys/stat.h>

int addx_builtin(WORD_LIST * list) {
	if (!list) {
		builtin_usage();
		return EX_USAGE;
	}
	puts("Adding execution permissions to this file:");
	puts(list->word->word);
	return chmod(list->word->word, S_IXUSR | S_IXGRP | S_IXOTH);
//	return EXECUTION_SUCCESS;
}

char *addx_doc[] = {
	"addx FILE",
	"Adds execute permissions to FILE. Equivalent to chmod +x FILE",
	(char *)NULL
};

struct builtin addx_struct = {
	.name = "addx", // name of builtin
	.function = addx_builtin, // function implementing it
	.flags = BUILTIN_ENABLED, // initial flags for builtin
	.long_doc = addx_doc,
	.short_doc = "addx FILE", // usage
	.handle = 0 // reserved for internal use
};
