---
title: Pipewire + Linux Sound
author:
  - Robert Detjens
  - OSU LUG
date: 3/3/22

theme: white
# controls: false
hash: true

width: 1600
height: 1000
---

# Sound is hard.

- Quality
- Latency
- Easy

# History

- OSS
- ALSA
- PulseAudio
- JACK
- Pipewire

# In the beginning

There was OSS

## Open Sound System

![Written for these sort of cards](images/Creative-Sound-Blaster-16-ASP-16-bit-ISA-card-CT-1740.jpg)

## Open Sound System

Available for every UNIX under the sun

Audio device driver, exposed as `/dev/dsp`

```sh
cat /dev/random > /dev/dsp    # plays white noise through the speaker
cat /dev/dsp > my_recording   # reads data from the microphone into a file
```

Assumed hardware for mixing streams, which was less common

Userspace mixing libraries (`sdl`, `libao`)

Synchronous

OSS v4 went proprietary, v3 deprecated in kernel in favor of...

# ALSA

Advanced Linux Sound Architecture

![](https://alsa-project.org/main/images/alsalogo.gif)

## ALSA

Started in 1998

Introduced to kernel in 2.5.5 (2002)

Default in 2.6 (2003)

## ALSA

- Allowed multiple cards, multiple devices
- USB
- MIDI
- Software mixing (kinda sucked)
- Complicated API (multi-threading!)
- New drivers, buggy hardware
- Worked alright for one app at a time

# Problems with ALSA / OSS

- work best with one client
- mixing sucks
- complicated
- static
- routing

# PulseAudio

![](https://www.freedesktop.org/software/pulseaudio/logo.png)

another Poettering project!

Userspace **sound server**

Runs on top of ALSA:

- things talk to PA
- PA mixes/resamples to one stream
- One stream sent to ALSA

## Benefits

- Better mixing
- Resampling
- Network transparency
- Routing
- App-level mixing
- Bluetooth
- GUI config

## Early problems

- Yet another big API change
- Buggy
  - timer-based scheduling
  - buffer 'flows
- Latency!
- etc...

## Why?

- power usage
  - low latency || low power
- buggy drivers

# JACK

![](https://linuxaudio.github.io/libremusicproduction/html/sites/default/files/articles/Jacklogo.png)

Pro audio focused audio server

Low low latency

Time sync

Plugins! DAW! MIDI!

Arbitrary connections, route anything into anything

## But...

Not desktop focused

Needs more configuration

Manual routing

# The new player

![](https://upload.wikimedia.org/wikipedia/en/thumb/3/37/Pipewire_logo.svg/2880px-Pipewire_logo.svg.png)

## Pipewire

Originally just for video streams in 2015

Video / webcams have similar problems as audio

"the PulseAudio for video"

Audio added in 2017/18

## Improvements

Unification of A & V

ACLs, Portal security

Generally smoother

Bluetooth is hassle-free!

Patchbay for plebians!

## How to get?

Fedora 34+: ✅

Arch:

```
pacman -Sy pipewire pipewire-alsa pipewire-pulse pipewire-jack wireplumber
```

**Less supported:**

Debian 11: <https://wiki.debian.org/PipeWire#Debian_11>

Ubuntu 20.04: <https://ubuntuhandbook.org/index.php/2022/04/pipewire-replace-pulseaudio-ubuntu-2204/>

Older Debian/Ubuntu: <https://pipewire-debian.github.io/pipewire-debian/>
