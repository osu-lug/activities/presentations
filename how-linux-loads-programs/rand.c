#include <stdio.h>  // printf
#include <stdlib.h> // sdrand, rand
#include <time.h>   // time

int main() {
	printf("I'm going to seed the random number generator\n");
	srand(time(NULL));

	printf("And now I'll fetch a random number\n");
	int r = rand();

	printf("I picked %d from a uniform distribution!\n", r);
	return 0;
}
