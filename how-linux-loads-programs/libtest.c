// why don't we need to #include <stdio.h>?

// the following must be compiled with options -fPIC -shared

int srand(int seed) {
	printf(">> Attempt to seed generator with %i\n", seed);
	return 0;
}


int rand() {
	printf(">> Attempt to get rand, but I'll give 12345678!\n");
	return 12345678;
}


// the following must be compiled with the -ldl option
#define _GNU_SOURCE
#include <dlfcn.h>
typedef int (*orig_open_f_type)(const char *pathname, int flags);

int open(const char *pathname, int flags, ...) {
	printf("Just tried to open %s\n", pathname);

	orig_open_f_type orig_open;
	orig_open = (orig_open_f_type)dlsym(RTLD_NEXT,"open");
	return orig_open(pathname,flags);
}
