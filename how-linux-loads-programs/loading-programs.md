---
title:  How Linux Loads Programs
author: Gabriel Kulp
date:   October 21, 2021
theme:  white
transition: concave
progress: false
controls: false
---

# Goals

- Make you feel welcome
- Starting points to explore
- Help CTF people
- Learn how *your* computer works

# Overview

- Shell and `PATH`
- `system()` and `exec()`
- `fork`, `execve`, and interrupts
- Interpreters, ELF, and `ld`
- Static and dynamic linking
- Virtual memory
- Filesystems
- Block devices
- Device drivers

# This talk

1. The `PATH` variable
2. Interpreters, ELF, and `ld`
3. Libraries and interrupts
4. Dynamic linking
5. Filesystems

# Topic 1:
## The `PATH` variable

---

this is probably easier with a demo

---





# Topic 2:
## Interpreters, ELF, and `ld`

# Interpreters

What's with `#!/bin/bash`?

Where does the `#!` get read?

What about putting something else there?

(quick demo)

---

## Ways to execute

1. File starts with `#!`
2. `/proc/sys/fs/binfmt_misc`
3. ELF (starts with 0x7f ELF)
4. Some legacy stuff

---

ELF header starts with "magic"

```
$ hexdump -C whoami | head

00000000  7f 45 4c 46 02 01 01 00  00 00 00 00 00 00 00 00
00000010  03 00 3e 00 01 00 00 00  f0 18 02 00 00 00 00 00
```

---

[Wikipedia ELF page](https://en.wikipedia.org/wiki/Executable_and_Linkable_Format)

```
00000000  7f 45 4c 46 02 01 01 00  00 00 00 00 00 00 00 00
00000010  03 00 3e 00 01 00 00 00  f0 18 02 00 00 00 00 00
```

Lots of info:

- ABI: Linux, OpenBSD, or Nintendo Wii?
- ISA: x86? ARM? RISC-V? MIPS?
- 32-bit, 64-bit, 128-bit?
- Little-endian? Big-endian?
- Dynamic linking required?

---

What's the goal, exactly?

Let's think about it backwards.

---

![](https://1.bp.blogspot.com/--K44cqPMBLE/U9YysG3by-I/AAAAAAAACC4/GS_Q_vPLU3g/s1600/memlayout.png){ width=50% }

---

The main show (for us):

## sections

"this part of file goes in this part of memory"

---

![](https://upload.wikimedia.org/wikipedia/commons/7/77/Elf-layout--en.svg){ width=50% }

---

`.data` and `.text` are most important.

Lots of others, though:

- `.rodata`
- `.bss`
- `.comment`
- `.dynamic`

---

Demo: rand.c

Tools: `hexdump`, `readelf`

# Topic 3:
## Libraries and Interrupts

---

## `#include <stdio.h>`

What does this do?

---

![](https://upload.wikimedia.org/wikipedia/commons/5/5b/Linux_kernel_map.png)

---

## basic structure:

1. Your program
2. Libraries (optional)
3. C library (*technically* optional)

**Interrupt 0x80**

4. System call (kernel ABI)
5. Kernel syscall implementation

---

## Demo

- show `ldd`
- show `strace` and `ltrace`
- talk about interrupts/system calls
- show `~/week3/stack-ovfl-sc-32`

# Topic 4:
## Dynamic Linking

---

Let's look at `rand.c` again.

`srand` and `rand` are define somewhere else!

---

What can you do with this?

- Functions defined elsewhere
- Program doesn't quite know `rand()`
- Load functions from anywhere?

---

Demo: our own library!

# Topic 5:
## Filesystems

---

![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Open_hard-drive.jpg/688px-Open_hard-drive.jpg)

---

![](https://www.servethehome.com/wp-content/uploads/2020/08/SNV3400-400G.jpg)

---

What about a really big book?

---

![](https://sarwiki.informatik.hu-berlin.de/images/b/b3/Lfs-layout-classic.png)

---

Layers (any FS)

1. Applications
2. Virtual filesystem (metadata)
3. File organization (mounts, partitions)
4. Basic filesystem
5. I/O and drivers
6. Physical device

---

EXT4 block types (simplified):

1. boot sector
2. super block
3. inode bitmap
4. inodes
5. data

---

Demo:

- `mount` and `fdisk`
- `cp`, `ln`, `mkdir`, `stat`




# Conclusion

it's pretty whack.

questions expected.
