---
title:  Pandoc, Markdown, & LaTeX
author:
- Robert Detjens
- OSU LUG
date:   October 28, 2021
theme:  white
progress: false
controls: false
---

# Documents

PDF is inescapable.

## Word

![](images/word.png){ height=60% }

## LibreOffice

![](images/libreoffice.png){ height=60% }

## What if?

Beautiful documents

::: incremental
- that are `git` friendly?
- without messing with formatting?
- automatic citations?
- minimal effort?
- more power when needed?
:::

## You bet!

![](images/vscode.png){ height=60% }

# Enter Pandoc

:::::: {.columns}
::: {.column width="50%"}
From:

- Markdown
- LaTeX
- EPUB
- Word DOCX
- Wiki markup
- HTML
- LaTeX
- (+ more)
:::
::: {.column width="50%"}
To:

- <-- All those
- PDF
- PowerPoint
- HTML presentations
- Manpage
- (+ even more)
:::
::::::

## Focus

Markdown -> PDF

# Markdown?

You've probably used it before

:::::: {.columns}
::: {.column width="50%"}

```md
# Header 1

- Bullet points
- *Italics*
- **Bold**
- `inline code`

## Header 2

#. Numbered lists
#. [Link](https://the.url)
#. ![Image](path/or/url.png)
```

:::
::: {.column width="50%"}

````md
| This is | a table header |
|---------|----------------|
| These   | are rows       |
| These   | are rows       |

```python
def code_block():
    print("pretty neat")
```

And some other stuff too
that we'll get to later
````
:::
::::::

## Renders nicely

:::::: {.columns}
::: {.column width="50%"}

**Example**

- Bullet points
- *Italics*
- **Bold**
- `inline code`

**Example**

#. Numbered lists
#. [Link](https://the.url)
#. ![not an actual image](path/or/url.png)


:::
::: {.column width="50%"}

| This is | a table header |
|---------|----------------|
| These   | are rows       |
| These   | are rows       |

```python
def code_block():
    print("pretty neat")
```

And some other stuff too
that we'll get to later

:::
::::::

# The fancy stuff

Pandoc has some extra things it can do

## Metadata

Initial YAML block at the start of the document

```md
---
title: An Example Document
author: Robert Detjens
date: 10/28

header-includes:
- \usepackage{setspace}
- \doublespacing
fontsize: 11pt
---

The rest of the document is in Markdown as normal,
and would have a title page with the set info and
be double spaced 11pt when rendered to PDF.
```

## LaTeX Math

Surround it with $s:

`$$\int \frac{5x + 6}{3} dx$$`

![](images/equation.png)

(more on this in part 2)

## Citations

```sh
pandoc -s example.md -o example.pdf --citeproc
                    # if on ubuntu, --filter=pandoc-citeproc
```

:::::: {.columns}
::: {.column width="50%"}

```md
---
bibliography: example.bib
csl: apa.csl
---

This is just some sample text
the make this look like an
actual paper [@example].

According to @example, you can
also do inline citations.
```

:::
::: {.column width="50%"}

```bibtex
@misc{example,
  title     = {Example entry},
  author    = {Bar, Foo},
  journal   = {Example Site},
  month     = {Oct},
  publisher = {Nobody},
  year      = {2021}
}
```

:::
::::::

## BibTeX

```yml
bibliography: example.bib
```

:::::: {.columns}
::: {.column width="50%"}
OSU Library search

![](images/library-bibtex.png)
:::
::: {.column width="50%"}
Citation Machine

![](images/cm-bibtex.png)
:::
::::::

## CSL

```yml
csl: apa.csl
```

CSLs are styles for BibTeX, from the [CSL Github repo](https://github.com/citation-style-language/styles)

Tells Pandoc how to format citations & Works Cited

Need to use a different style? Just swap it out!

# How to install?

## Linux

```sh
# Arch / Manjaro
sudo pacman -Sy pandoc texlive-most

# Ubuntu & derivatives
sudo apt install pandoc pandoc-citeproc \
                 texlive texlive-latex-extra

# Fedora
sudo dnf install pandoc pandoc-citeproc texlive
```

## Windows

Get the installer from [Github (`jgm/pandoc`)](https://github.com/jgm/pandoc/releases)

Or use WSL, this is the LUG after all ;)

# How to use?

```sh
pandoc $INPUT_FILE -o $OUTPUT_FILE
```

Make your lives easier with a shell alias:

```sh
md2pdf () {
  pandoc "$1" -o "${1%%.md}.pdf" -V geometry:margin=1in \
         --citeproc ${@:2}
  #      --filter=pandoc-citeproc instead on ubuntu / fedora
}

md2pdf some-document.md --number-sections
```

# For powerusers

Pandoc uses a set template for MD -> LaTeX -> PDF

If you need more control over the output...

<!-- switch to Gabriel's LaTeX pres -->
