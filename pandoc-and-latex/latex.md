---
title:  LaTeX is cool too
author: Gabriel Kulp
date:   October 28, 2021
theme:  white
transition: concave
progress: false
controls: false
---

# Goals

- What's the big idea?
- Learn how to collaborate
- Learn some basic syntax
- Learn how to learn

# Topic 1
## what's the big idea?

---

## background info

::: incremental
- Pronunciation???
- It's some libraries for TeX
- It's old
- Typeset anything!
:::

---

## design goals

::: incremental
- Everything is a file
- WYSIWYM, not WSYWYG
- Separate styling from content
- Consistency is default
- Optional abstractions, macros, programming
:::

---

## programming?

- Repetition made easy
- Calculate what to draw
- Lots done for you:
  - Citations
  - Table of contents
  - Spacing and kerning

# Topic 2
## how to collaborate

---

## templates

- Instructors have requirements
- Journals have requirements
- You might have requirements

---

## `git` friendly!

- Easy `diff` and `merge`
- Edit with anything
- Read document source in browser
- Separate sections into files 

---

# Topic 3
## some basic syntax

---

## document structure

main.tex:
```latex
\documentclass{article}
% header goes here

\begin{document}

% document goes here

\end{document
```

---

## main structure

```latex
\section{Introduction}

\subsection{Motivation}
You just mix text right in with these statements like this.

Leave empty line for paragraph break

\subsection{Scope}

\subsubsection{Why}

\include{file.tex}
```

---

## formatting

```latex
I can make things \textbf{bold} and \textit{italics}.

\begin{itemize}
  \item Bulleted lists
  \item Are like this
\end{itemize}

\begin{enumerate}
  \item This is item one
  \item This is item two
\end{enumerate}
```

---

## lots more!

::: incremental
- Math
- Tables
- Citations
- Footnotes
- Font size and color
- Pictures and plots
:::

# Walkthrough:
## my thesis paper

---

## Questions?

- Stack Overflow
- Overleaf tutorials
- First-party documentation
