% Introduction to Git and Github
% OSULUG -- Robert Detjens
% 10/14/21

# What on earth is `git`?

## version control!

created in 2005
by Linus Torvalds
for the Linux kernel

# What on earth is version control?

## Avoids this:

- `hw_final.doc`
- `hw_final2.doc`
- `hw_final_no_really_this_time.doc`
- `hw_aaaaaaaaaa.doc`

## Instead:

- a "commit" saves the state of the repo (the folder being tracked)
- keeps track of changes between commits
- can revert back to any previous state
- somewhat like a linked list of diffs

## Useful for:

- source code
- notes
- homework
- anything that needs backups

# OK, thats cool, but how do I use this?

## Several GUI frontends

- Github Desktop
- GitKraken
- various editor integrations

## One command to rule them all

`git`

## Basic stuff

- `git clone`
- `git status`
- `git add`
- `git commit`
- `git push`

## `git clone`

![](images/clone.png)

Downloads an existing repo to a local folder

Creates a local copy or *clone*

## `git status`

Shows the current **status** of the repo

![no changes since last commit](images/status-clean.png)

## `git status`

If any files have been changed since the last commit, this will show which ones

![](images/status-dirty.png)

## `git diff`

Shows what has changed in those files (the *diff*erence)

![](images/diff.png){ width=50% }

## `git add`

Tells git to add some changed files to the next commit

```sh
# add all changed files
$ git add --all

# or just specific ones
$ git add foo.md bar.md images/clouds.png
```

## `git add`

`git status` will show what files have been added

![](images/status-add.png)

## `git commit`

Actually saves the added files' changes to the repo

This will open up `$EDITOR` for a commit message

![](images/commit.png)

## `git log`

Shows the commit history

![](images/log.png)

## `git push`

Sends commits to the remote repo

```
$ git push
Enumerating objects: 333, done.
Counting objects: 100% (333/333), done.
Delta compression using up to 12 threads
Compressing objects: 100% (122/122), done.
Writing objects: 100% (326/326), 54.25 KiB | 54.25 MiB/s, done.
Total 326 (delta 193), reused 313 (delta 184), pack-reused 0
remote: Resolving deltas: 100% (193/193), completed with 5 local objects.
```

# GitHub

## What is GitHub?

- Git repo hosting
- Web interface for repo management
- Adds collaboration features
  - issues
  - easy **pull requests**
  - CI
  - etc

## How use?

Create a repo and clone it locally

Edit the repo as shown before

Push committed changes back to GitHub

## What is a pull / merge request?

A way to request that a branch is merged into another

# Branches

## Branch? Merge? What?

![`git` has the concept of branches](https://blog.seibert-media.net/wp-content/uploads/2015/07/Git-Branches-1.png){ height=30% }

## Working with branches

```
$ git checkout -B a-new-branch
```

Commits are added to the current branch

`git status` shows the current branch:

```
$ git status
On branch a-new-branch

nothing to commit, working tree clean
```

## Branch workflow

- make new branch
- add feature / bugfix / etc to branch
- make PR to merge branch into `main`
- reviewed & merged

## Merging

Branches can be combined with a **merge**

## Feature branch

![](https://wac-cdn.atlassian.com/dam/jcr:7afd8460-b7bf-4c42-b997-4f5cf24f21e8/01%20Branch-2%20kopiera.png?cdnVersion=1852)

## Merging back into `main`

![](https://wac-cdn.atlassian.com/dam/jcr:c6db91c1-1343-4d45-8c93-bdba910b9506/02%20Branch-1%20kopiera.png?cdnVersion=1852)

## Rebasing

Branches "branch off" from an original branch

![Rebasing moves that point to a different commit](https://blogs.atlassian.com/wp-content/uploads/rebase-diagram-adg3.png){ width=40% }

## Merge conflicts

What happens when two commits try to modify the same file?

`git` doesn't know what to do and stops for manual resolution

-- demo --

# Any questions?
