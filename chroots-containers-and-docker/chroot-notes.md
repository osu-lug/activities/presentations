## history and purpose

Started in 1979 in Unix v7, and was added to BSD in 1982. Not a lot of interest or development in containers at the time, that's that.

`chroot` restricts a process' view of the filesystem to a particular branch. Basically it provides a new `/` (filesystem root) and prevents `cd ..` from there.

## a simple example

First, we need a static binary. (What does that mean? See my talk on [How Linux Loads Programs](https://gitlab.com/osu-lug/activities/presentations/-/tree/main/how-linux-loads-programs)!)

```sh
mkdir simple
cd simple
wget https://www.busybox.net/downloads/binaries/1.21.1/busybox-x86_64
mv busybox* busybox # give it a nicer name
chmod +x busybox # make it executable
```

Next, we can execute this program: `./busybox` and it will tell us all about what it can do.

`pwd` versus `./busybox pwd` versus `chroot . ./busybox pwd`

How about `sh` instead of `pwd`?

```sh
chroot . ./busybox sh

# which of these work? why?
ls
echo hello?
echo $PATH
echo testing > file
cat file
/busybox cat file
```

Busybox is cool. It's minimal versions of all the common commands bundled into one program. There are two ways to use it: `./busybox <cmd>` and by renaming `busybox` to the command you want to run. Let's use a symbolic link! (Learn about those in [How Linux Loads Programs](https://gitlab.com/osu-lug/activities/presentations/-/tree/main/how-linux-loads-programs)!)

```sh
/busybox mkdir -p /usr/bin
/busybox ln -s /busybox /usr/bin/ln
ln
ln -s /busybox /usr/bin/ls
ls
ln -s /busybox /usr/bin/mv
mv test test2
ln -s /busybox /usr/bin/cat
cat test2
# etc.
```

Since `sh` looks inside `/usr/bin` for programs, we can put named links to Busybox in there! Busybox has a self-install feature with lots of options. This automates what we were just doing:

```sh
/busybox --install -s /usr/bin
wget
```

And now you have a simple Linux system! It has no package manager, but it has `wget` for downloading things, so you could download `gcc` and bootstrap from there.

```sh
exit
cd ..
```

## isolating a program

How about we isolate an existing process instead of building a new thing in isolation? Let's try to make a container to run `nano`.

```sh
mkdir barebones
cd barebones
cp /usr/bin/nano .
chroot . ./nano
```

What's going on here? Busybox worked because it's a static executable, but (this version of) `nano` is not, so it needs to read libraries when it starts. Lets see which ones:

```sh
ldd nano
```

Watch my talk [How Linux Loads Programs](https://gitlab.com/osu-lug/activities/presentations/-/tree/main/how-linux-loads-programs) to learn more about this! Let's start by copying the dynamic linker:

```sh
mkdir lib64
# this command is needlessly complex to accommodate more distros
ldd nano | cut -d\  -f 3 | grep ld-linux | xargs -I {} cp {} ./lib64
```

```sh
mkdir -p usr/lib
# copy libraries from ldd into ./usr/lib (it's ok if this is magic)
ldd nano | cut -d\  -f 3 | grep -v ld-linux | xargs -I {} cp {} ./usr/lib
```

Note that the paths could be different in different distros. I only checked on Manjaro, Ubuntu, and the ARM version of Debian. Debian used `lib` instaed of `lib64` in the command above. Just copy whatever it says in `ldd`.

And now we can start `nano` in the chroot!

```sh
chroot . ./nano --version
chroot . ./nano
```

Looks like it can't find something it needs. If you're adventurous, you can dig through the output of `strace chroot . ./nano` to see that it tried to read `/usr/share/terminfo` and failed. The specifics of what's in there is a talk for another time :) so let's just fix it and move on:

```sh
mkdir -p usr/share/terminfo/x/
cp /usr/share/terminfo/x/xterm-256color usr/share/terminfo/x/

chroot . ./nano
```

Yay! You can still make it self-destruct by writing over one of its own files, but at least you can't break anything about the host system. You might imagine there are ways to make this read-only to prevent that.

This procedure is hard! And worse for more complex programs. Google has an open-source project called [`distroless`](https://github.com/GoogleContainerTools/distroless) for making Docker images that are stripped out like this.

```sh
cd ..
```

## running a full distro

Download appropriate rootfs from [Alpine Linux downloads](https://alpinelinux.org/downloads/) and then extract it:

```sh
wget https://dl-cdn.alpinelinux.org/alpine/v3.14/releases/x86_64/alpine-minirootfs-3.14.2-x86_64.tar.gz
mkdir alpine; cd alpine
tar zxf ../alpine-minirootfs*
```
Now you can run a new command inside it with `chroot . $COMMAND`, where `$COMMAND` is the thing you want to run. To get a shell, use `chroot . bin/sh -l` where the `-l` makes `/bin/sh` behave as a login shell, setting variables and such appropriately.

Let's try to install and run `htop` as an example. Here's how to install a package on Alpine:

```sh
apk update
apk add htop
```

Doesn't work, though. Why not? Do we have internet?

```sh
ping 1.1.1.1 -c 4
ping osuosl.org -c 4
```

Looks like we have internet, but no name resolution. Let's poke around a bit more and see what's going on:

```sh
uname -a # what system is this?
df -h    # how much storage space is available?
mount    # what storage is mounted?
free     # how much memory is in use?
ls /dev  # what devices are available?
exit     # leave chroot to make some changes
```

Clearly some things are missing. Thankfully, you can add back what you need!

```sh
# get the nameserver domain name resolution
cp -L /etc/resolv.conf etc

# mount /dev exactly as the host sees it
mount /dev dev --bind
mount -o remount,ro,bind dev

# mount /proc by making a new one
mount -t proc proc ./proc -o nosuid,nodev,noexec

# if you want a "full" experience (not needed for demo)
# then check out https://wiki.alpinelinux.org/wiki/Chroot

# hop back in chroot to poke around!
chroot . bin/ash -l
```

And now that we have a more complete setup, let's try those commands again!

```sh
apk update
apk add htop
htop
# leave htop with q, F10, or Ctrl+C

df -h    # how much storage space is available?
mount    # what storage is mounted?
free     # how much memory is in use?
ls /dev  # what devices are available?
```

You now have a sandbox! And you can build and test software for another distro!

Since `/dev` is identical in the chroot and on the host, and you're root inside the chroot, you can still mess things up. This is not a VM.

But instead of mounting all of `/dev`, `/proc`, etc. you could just make certain parts inside the chroot.

This is a really basic way to do isolation, but it's not super secure. Good for testing unknown behavior, but not adequate for protecting against malicious behavior.

This whole process was a bit of a pain, but it has been automated for many scenarios. Here are some:

- `mkarchroot` for [building packages in Arch Linux](https://wiki.archlinux.org/title/DeveloperWiki:Building_in_a_clean_chroot)
- `debootstrap` for [setting up](https://wiki.debian.org/Debootstrap) a functional Debian installation
- `pmbootstrap` for installing [postmarketOS](https://wiki.postmarketos.org/wiki/Installing_pmbootstrap)

Docker uses a kernel feature called [LXC](https://linuxcontainers.org/lxc/introduction/) (LinuX Containers), which combines chroot with other types of isolation.